using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    [Header("ROTATION")]
    [SerializeField] private float rotationSpeed = 50f;
    [NonSerialized] private float startMousePositionX;

    [Header("ZOOM")]
    [SerializeField] private float movementSpeed = 2.5f;
    [SerializeField] private float minZoom = 1f;
    [SerializeField] private float maxZoom = 1.5f;
    [NonSerialized] private float currentZoom = 1f;
    [NonSerialized] public bool rotatingCamera = false;

    [Header("Walls")]
    [SerializeField] private GameObject w1;
    [SerializeField] private GameObject w2;
    [SerializeField] private GameObject w3;
    [SerializeField] private GameObject w4;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startMousePositionX = Input.mousePosition.x;
        }
        else if (Input.GetMouseButton(0))
        {
            rotatingCamera = true;
            RotateObject();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            rotatingCamera = false;
        }
        CheckRotationForWalls();
    }

    private void RotateObject()
    {
        float mouseDeltaX = Input.mousePosition.x - startMousePositionX;
        float rotationAmount = mouseDeltaX * rotationSpeed * Time.deltaTime;
        transform.Rotate(0f, -rotationAmount, 0f);
        startMousePositionX = Input.mousePosition.x;
    }

    private void CheckRotationForWalls()
    {
        float rotationY = transform.rotation.eulerAngles.y;
        if (rotationY > 0 && rotationY < 45)
        {
            w4.SetActive(false);
            w1.SetActive(true);
            w2.SetActive(true);
            w3.SetActive(false);
        }
        else if (rotationY > 45 && rotationY < 90)
        {
            w1.SetActive(false);
            w2.SetActive(true);
            w3.SetActive(true);
            w4.SetActive(false);
        }
        else if (rotationY > 135 && rotationY < 180)
        {
            w2.SetActive(false);
            w3.SetActive(true);
            w4.SetActive(true);
            w1.SetActive(false);
        }
        else if (rotationY > 225 && rotationY < 270)
        {
            w3.SetActive(false);
            w4.SetActive(true);
            w1.SetActive(true);
            w2.SetActive(false);
        }
    }

    private void ScaleObject()
    {
        float scrollDelta = Input.GetAxis("Mouse ScrollWheel");
        currentZoom += scrollDelta * movementSpeed;
        currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);
        transform.localScale = new Vector3(currentZoom, currentZoom, currentZoom);
    }
}