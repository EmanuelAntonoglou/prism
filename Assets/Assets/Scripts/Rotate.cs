using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    private void OnMouseDown()
    {
        RotateObject();
    }

    private void RotateObject()
    {
        Vector3 rotation = new Vector3(0, 90, 0);
        transform.parent.Rotate(rotation);
    }
}
