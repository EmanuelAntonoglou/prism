using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class LaserController : MonoBehaviour
{
    [Header("Laser Data")]
    [SerializeField] private LayerMask mirrorLayer;
    [SerializeField] private int maxBounces = 5;
    [SerializeField] private float maxDistance = 20f;
    [SerializeField] private float laserRadius = 0.25f;
    [NonSerialized] private List<GameObject> previousCylinders = new List<GameObject>();

    [Header("References")]
    [SerializeField] private GameObject laserPrefab;
    [SerializeField] private GameObject laserSegments;

    [Header("Components")]
    [NonSerialized] private LineRenderer lr;
    [NonSerialized] private Material laserMat;
    

    void Start()
    {
        lr = GetComponent<LineRenderer>();
        laserMat = GetComponent<Renderer>().material;
        lr.positionCount = maxBounces + 1;
    }

    void Update()
    {
        if (!GameManager.i.cameraController.rotatingCamera)
        {
            CastLaser(transform.position, transform.right);

            if (true) //Check for Changes
            {
                //CreateLaser();
            }
        }
    }

    private void CastLaser(Vector3 pos, Vector3 dir)
    {
        lr.SetPosition(0, transform.position);
        int segmentsUsed = 1;

        for (int i = 0; i < maxBounces; i++)
        {
            if (Physics.Raycast(new Ray(pos, dir), out RaycastHit hit, maxDistance, mirrorLayer))
            {
                pos = hit.point;
                dir = Vector3.Reflect(dir, hit.normal);
                lr.SetPosition(segmentsUsed, hit.point);
                segmentsUsed++;
            }
            else
            {
                lr.SetPosition(segmentsUsed, pos + dir * maxDistance);
                segmentsUsed++;
                break;
            }
        }
        lr.positionCount = segmentsUsed;

        CreateLaser();
    }

    private void CreateLaser()
    {
        // Destroy previously created cylinders (if any)
        for (int i = laserSegments.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(laserSegments.transform.GetChild(i).gameObject);
        }
        previousCylinders.Clear();

        // Create cylinders for each segment of the LineRenderer
        for (int i = 1; i < lr.positionCount; i++)
        {
            CreateLaserSegment(lr.GetPosition(i - 1), lr.GetPosition(i));
            previousCylinders.Add(laserSegments.transform.GetChild(i - 1).gameObject);
        }
    }

    private void CreateLaserSegment(Vector3 startPoint, Vector3 endPoint)
    {
        GameObject cylinderSegment = Instantiate(laserPrefab, transform);
        cylinderSegment.GetComponent<Renderer>().material = laserMat;
        cylinderSegment.transform.SetParent(laserSegments.transform);

        // Calculate the direction vector
        Vector3 direction = endPoint - startPoint;

        // Calculate the distance between points A and B
        float distance = Vector3.Distance(startPoint, endPoint);

        // Define a small multiplier for slight adjustment (adjust as needed)
        float adjustmentMultiplier = 1.5f; // Experiment with values slightly above 1

        // Calculate adjusted start and end positions
        Vector3 adjustedStartPoint = startPoint - direction.normalized * laserRadius * adjustmentMultiplier;
        Vector3 adjustedEndPoint = endPoint + direction.normalized * laserRadius * adjustmentMultiplier;

        // Calculate the adjusted distance between adjusted points
        float adjustedDistance = Vector3.Distance(adjustedStartPoint, adjustedEndPoint);

        // Calculate the required cylinder height to reach adjusted points
        float requiredHeight = adjustedDistance - 2f * laserRadius; // Subtract 2 radii for offset

        // Set the cylinder scale (height and radius)
        cylinderSegment.transform.localScale = new Vector3(laserRadius, requiredHeight, laserRadius);

        // Position the cylinder at the midpoint of adjusted points
        Vector3 middlePoint = (adjustedStartPoint + adjustedEndPoint) / 2f;
        cylinderSegment.transform.position = middlePoint;

        // Set cylinder orientation
        cylinderSegment.transform.up = direction;
    }

}

