using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererController : MonoBehaviour
{
    [Header("Laser Data")]
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private LayerMask collisionLayer;
    [NonSerialized] private LineRenderer lineRenderer;
    [NonSerialized] private int lastPointIndex = 1;
    [NonSerialized] private Vector3 originalDirection = Vector3.right;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Start()
    {
        AddPoint();
    }

    void Update()
    {
        MoveLinePoint();
    }

    private void MoveLinePoint()
    {
        Vector3 startPoint = lineRenderer.GetPosition(lastPointIndex - 1);
        Vector3 endPoint = lineRenderer.GetPosition(lastPointIndex);

        RaycastHit hit;
        if (Physics.Raycast(startPoint, endPoint - startPoint, out hit, Vector3.Distance(startPoint, endPoint), collisionLayer))
        {
            AddPoint();
            lineRenderer.SetPosition(lastPointIndex, hit.point);

            Vector3 direction = Quaternion.Euler(0, 90, 0) * (hit.point - startPoint).normalized;
            endPoint = hit.point + direction * speed * Time.deltaTime;
            lineRenderer.SetPosition(lastPointIndex, endPoint);
            originalDirection = direction;
        }
        else
        {
            Vector3 newPos = endPoint + originalDirection * speed * Time.deltaTime;
            lineRenderer.SetPosition(lastPointIndex, newPos);
        }
    }

    void AddPoint()
    {
        lineRenderer.positionCount++;
        lastPointIndex = lineRenderer.positionCount - 1;
    }
}